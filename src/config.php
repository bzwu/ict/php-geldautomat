<?php

return [
	'cliPrefix'    => 'Input',
	'allowedCoins' => [0.05, 0.10, 0.20, 0.50, 1, 2, 5]
];