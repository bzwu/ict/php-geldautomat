<?php

namespace Geldautomat\Utilities;

class CliHelper
{
	/**
	 * The config as array
	 *
	 * @var null|array
	 */
	private static $_config = null;

	/**
	 * Checks if the required parameters from
	 * the commandline are given
	 *
	 * @param array $cliArgs
	 *
	 * @return bool
	 */
	public static function checkArgs( $cliArgs )
	{
		if ( count( $cliArgs ) === 2 ) {

			return true;
		}

		return false;
	}

	/**
	 * Returns the configuration as array
	 *
	 * @return array
	 */
	public static function getConfig()
	{
		if ( self::$_config === null ) {
			self::$_config = include PROJECT_ROOT . '/config.php';
		}

		return self::$_config;
	}

	/**
	 * Return the commandline prefix
	 * which is set in the config
	 *
	 * @return string
	 */
	private static function getCliPrefix()
	{
		return '[' . self::getConfig()['cliPrefix'] . '] ';
	}

	/**
	 * Read an input from the commandline
	 *
	 * @param string $text
	 *
	 * @return string
	 */
	public static function input( $text )
	{
		return readline( self::getCliPrefix() . $text );
	}

	/**
	 * Print out an error message on the commandline
	 *
	 * @param string $text
	 *
	 * @return void
	 */
	public static function error( $text )
	{
		echo PHP_EOL . '[ERROR] ' . $text . PHP_EOL . PHP_EOL;
	}

	/**
	 * Print out an info message
	 *
	 * @param string $text
	 *
	 * @return void
	 */
	public static function info( $text )
	{
		echo '[INFO] ' . $text . PHP_EOL;
	}

	/**
	 * Print out a debug message
	 *
	 * @param string $text
	 *
	 * @return void
	 */
	public static function debug( $text )
	{
		ob_start();
		var_dump( $text );
		$debug = ob_get_clean();

		echo '[DEBUG] ' . $debug . PHP_EOL;
	}
}