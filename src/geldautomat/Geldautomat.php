<?php

namespace Geldautomat;

use Geldautomat\Utilities\CliHelper;

class Geldautomat
{
	/**
	 * Save the products price which is given
	 * as commandline parameter
	 *
	 * @var float
	 */
	private $productPrice;
	
	/**
	 * Constructor - Check if the given price is
	 * in a valid form
	 *
	 * @param int $productPrice
	 */
	public function __construct( $productPrice )
	{
		$error    = false;
		$fraction = 0;
		
		if ( preg_match( '/\./', (string)$productPrice ) ) {
			$splitted = explode( '.', (string)$productPrice );
			if ( count( $splitted ) > 2 ) {
				$error = true;
			}
			$fraction = (int)rtrim( trim( $splitted[1] ), '0' );
		}
		
		if ( strlen( $fraction ) <= 2 ) {
			$arrCoins = CliHelper::getConfig()['allowedCoins'];
			sort( $arrCoins );
			$arrCoins = array_reverse( $arrCoins );
			
			$error = $this->validateCoins( $arrCoins, (float)$productPrice );
		} else {
			$error = true;
		}
		
		if ( $error ) {
			die( 'Your input seems to be invalid! The price must be able to be formed with the following coins: ' . implode( ', ', CliHelper::getConfig()['allowedCoins'] ) . PHP_EOL );
		}
		
		$this->productPrice = (float)$productPrice;
	}
	
	/**
	 * @param array $allowedCoins
	 * @param float $productPrice
	 *
	 * @return bool
	 */
	private function validateCoins( $allowedCoins, $productPrice )
	{
		$productPrice = (int)round( $productPrice * 100 );
		foreach ( $allowedCoins as $coin ) {
			$coin = (int)round( $coin * 100 );
			if ( $productPrice >= $coin ) {
				$rest = $productPrice % $coin;
				if ( $rest === 0 ) {
					
					return false;
				}
				
				$productPrice = $rest;
			}
		}
		
		return true;
	}
	
	/**
	 * Start the calculation/input process
	 *
	 * @return void
	 */
	public function start()
	{
		$insertedTotal = 0;
		
		while ( $insertedTotal < $this->productPrice ) {
			$inserted = (float)trim( CliHelper::input( 'Please insert your coins: ' ) );
			if ( !in_array( $inserted, CliHelper::getConfig()['allowedCoins'] ) ) {
				CliHelper::error( 'Your inserted coin is not accepted.' );
				CliHelper::info( 'Accepted coins: ' . implode( ', ', CliHelper::getConfig()['allowedCoins'] ) );
				
				continue;
			}
			
			$insertedTotal += $inserted;
			CliHelper::info( 'Subtotal: ' . $insertedTotal . '/' . $this->productPrice );
		}
		
		$difference = abs( $insertedTotal - $this->productPrice );
		
		if ( $difference !== 0 ) {
			CliHelper::info( 'Your change: ' . $difference . ' Fr.' );
		}
		
		die( 'Thank you for shopping today. Good bye!' . PHP_EOL );
	}
}